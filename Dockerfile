FROM --platform=linux/amd64 ubuntu:22.04

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get -y upgrade
RUN apt-get install -y software-properties-common
RUN add-apt-repository ppa:deadsnakes/ppa -y

RUN apt-get install -y git unzip wget sudo curl build-essential gettext \
    libmemcached11 libmemcachedutil2 libmemcached-dev libz-dev memcached \
    libproj-dev libfreexl-dev libgdal-dev gdal-bin wkhtmltopdf\
    ffmpeg

# install python 3.9
RUN curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y python3.9 python3.9-distutils python3-apt && \
    python3.9 get-pip.py

# install python 3.11
RUN curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y python3.11 python3.11-distutils python3-apt && \
    python3.11 get-pip.py

# install wkhtmltopdf for emails
RUN wget https://github.com/wkhtmltopdf/packaging/releases/download/0.12.6.1-2/wkhtmltox_0.12.6.1-2.jammy_amd64.deb && \
    apt install -y ./wkhtmltox_0.12.6.1-2.jammy_amd64.deb

ENV PYTHONIOENCODING=utf-8

# Pass this environment variables through a file
# https://docs.docker.com/engine/reference/commandline/run/#set-environment-variables--e---env---env-file
# They will be used to create a default database on start
